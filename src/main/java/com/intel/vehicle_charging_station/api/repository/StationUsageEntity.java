package com.intel.vehicle_charging_station.api.repository;

import lombok.Data;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDateTime;

@Entity(name = "electric_vehicle_charging_station_usage")
@Data
public class StationUsageEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String stationName;
    private String macAddress;
    private LocalDateTime startDate;
    private String startTimeZone;
    private LocalDateTime endDate;
    private String endTimeZone;
    /* Pacific Time */
    private LocalDateTime transactionDate;
    private Time totalDuration;
    private Time chargingTime;
    /* kWh */
    private Double energy;
    private String userId;
}
