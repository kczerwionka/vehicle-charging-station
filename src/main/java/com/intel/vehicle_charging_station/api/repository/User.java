package com.intel.vehicle_charging_station.api.repository;

import lombok.Value;

@Value
public class User {
    private String userId;
    private Double averageEnergyPerOneCharging;
    private Long numberOfCharging;
    private Double sumOfConsumedEnergy;
}
