package com.intel.vehicle_charging_station.api.repository;

import lombok.Value;

import java.util.Date;

@Value
public class DayInfo {
    public Date endDate;
    public String userIdWithMaxEnergyConsumption;
    public double energy;
}
