package com.intel.vehicle_charging_station.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface StationUsageRepository extends JpaRepository<StationUsageEntity, Long>,
        JpaSpecificationExecutor<StationUsageEntity> {
    @Query("SELECT " +
            "new com.intel.vehicle_charging_station.api.repository.User(sue.userId, avg(sue.energy), count(sue.energy), sum(sue.energy))" +
            "FROM electric_vehicle_charging_station_usage sue GROUP BY sue.userId")
    List<User> getUsers();

    @Query("SELECT " +
            "new com.intel.vehicle_charging_station.api.repository.User(sue.userId, avg(sue.energy), count(sue.energy), sum(sue.energy))" +
            "FROM electric_vehicle_charging_station_usage sue GROUP BY sue.userId HAVING sum(sue.energy) > ?1")
    List<User> getUsers(Double minEnergyConsumed);

    @Query("SELECT " +
            "new com.intel.vehicle_charging_station.api.repository.DayInfo(DATE(sue.endDate), sue.userId, sum(sue.energy)) " +
            "FROM electric_vehicle_charging_station_usage sue GROUP BY DATE(sue.endDate), sue.userId"
    )
    List<DayInfo> getDaysInfo(LocalDate dateFrom, LocalDate dateTo);
}
