package com.intel.vehicle_charging_station.api.service.exception;

public class StationUsageNotFoundException extends RuntimeException {
    public StationUsageNotFoundException(Long id) {
        super("Station usage not found for id: " + id);
    }
}
