package com.intel.vehicle_charging_station.api.service;

import com.intel.vehicle_charging_station.api.repository.StationUsageEntity;
import com.intel.vehicle_charging_station.api.repository.StationUsageEntity_;
import org.springframework.data.jpa.domain.Specification;

public class StationUsageSpecifications {
    public static Specification<StationUsageEntity> macAddressEquals(String macAddress) {
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(StationUsageEntity_.MAC_ADDRESS), macAddress));
    }

    public static Specification<StationUsageEntity> userIdEquals(String userId) {
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(StationUsageEntity_.USER_ID), userId));
    }

    public static Specification<StationUsageEntity> minEnergy(Double minEnergy) {
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(StationUsageEntity_.ENERGY), minEnergy));
    }

    public static Specification<StationUsageEntity> maxEnergy(Double maxEnergy) {
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(StationUsageEntity_.ENERGY), maxEnergy));
    }
}
