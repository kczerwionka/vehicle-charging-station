package com.intel.vehicle_charging_station.api.service;

import com.intel.vehicle_charging_station.api.repository.StationUsageRepository;
import com.intel.vehicle_charging_station.api.repository.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final StationUsageRepository stationUsageRepository;

    public UserService(StationUsageRepository stationUsageRepository) {
        this.stationUsageRepository = stationUsageRepository;
    }

    public List<User> getUsers() {
        return stationUsageRepository.getUsers();
    }
}
