package com.intel.vehicle_charging_station.api.service;

import com.intel.vehicle_charging_station.api.repository.StationUsageEntity;
import com.intel.vehicle_charging_station.api.repository.StationUsageRepository;
import com.intel.vehicle_charging_station.api.service.exception.StationUsageNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.intel.vehicle_charging_station.api.service.StationUsageSpecifications.*;
import static org.springframework.data.jpa.domain.Specification.where;

@Service
public class StationUsageService {
    private final StationUsageRepository stationUsageRepository;

    public StationUsageService(StationUsageRepository stationUsageRepository) {
        this.stationUsageRepository = stationUsageRepository;
    }

    public StationUsageEntity getById(Long id) {
        return stationUsageRepository.findById(id).orElseThrow(() -> new StationUsageNotFoundException(id));
    }

    public Page<StationUsageEntity> getAll(String macAddress, String userId, Double minEnergy, Double maxEnergy,
                                           Integer page, Integer size) {
        Specification<StationUsageEntity> specification = where(null);
        if (macAddress != null) {
            specification = specification.and(macAddressEquals(macAddress));
        }
        if (userId != null) {
            specification = specification.and(userIdEquals(userId));
        }
        if (minEnergy != null) {
            specification = specification.and(minEnergy(minEnergy));
        }
        if(maxEnergy != null) {
            specification = specification.and(maxEnergy(maxEnergy));
        }

        return stationUsageRepository.findAll(specification, PageRequest.of(page, size));
    }

    public StationUsageEntity create(StationUsageEntity stationUsageEntity) {
        return stationUsageRepository.save(stationUsageEntity);
    }
}
