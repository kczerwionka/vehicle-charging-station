package com.intel.vehicle_charging_station.api.service;

import com.intel.vehicle_charging_station.api.repository.DayInfo;
import com.intel.vehicle_charging_station.api.repository.StationUsageRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class DayInfoService {
    private StationUsageRepository stationUsageRepository;

    public DayInfoService(StationUsageRepository stationUsageRepository) {
        this.stationUsageRepository = stationUsageRepository;
    }

    public List<DayInfo> getDayInfo(LocalDate dateFrom, LocalDate dateTo) {
            Map<Date, DayInfo> result= this.stationUsageRepository.getDaysInfo(dateFrom, dateTo)
            .stream().collect(Collectors.toMap(DayInfo::getEndDate, Function.identity(),
                            BinaryOperator.maxBy(Comparator.comparing(DayInfo::getEnergy))));

        return new ArrayList<>(result.values()).stream().sorted(Comparator.comparing(DayInfo::getEndDate))
                .collect(Collectors.toList());
    }
}
