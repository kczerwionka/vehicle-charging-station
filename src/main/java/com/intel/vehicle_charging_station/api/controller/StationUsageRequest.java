package com.intel.vehicle_charging_station.api.controller;

import lombok.Data;

import java.sql.Time;
import java.time.LocalDateTime;

@Data
public class StationUsageRequest {
    private String stationName;
    private String macAddress;
    private LocalDateTime startDate;
    private String startTimeZone;
    private LocalDateTime endDate;
    private String endTimeZone;
    private LocalDateTime transactionDate;
    private Time totalDuration;
    private Time chargingTime;
    private Double energy;
    private String userId;
}
