package com.intel.vehicle_charging_station.api.controller;

import com.intel.vehicle_charging_station.api.repository.StationUsageEntity;
import org.springframework.stereotype.Service;

@Service
public class StationUsageMapper {
    public StationUsageEntity map(StationUsageRequest stationUsageRequest) {
        StationUsageEntity stationUsageEntity = new StationUsageEntity();
        stationUsageEntity.setStationName(stationUsageRequest.getStationName());
        stationUsageEntity.setEndDate(stationUsageRequest.getEndDate());
        stationUsageEntity.setEndTimeZone(stationUsageRequest.getEndTimeZone());
        stationUsageEntity.setStartDate(stationUsageRequest.getStartDate());
        stationUsageEntity.setStartTimeZone(stationUsageRequest.getStartTimeZone());
        stationUsageEntity.setTransactionDate(stationUsageRequest.getTransactionDate());
        stationUsageEntity.setEnergy(stationUsageRequest.getEnergy());
        stationUsageEntity.setUserId(stationUsageRequest.getUserId());
        stationUsageEntity.setMacAddress(stationUsageRequest.getMacAddress());
        stationUsageEntity.setChargingTime(stationUsageRequest.getChargingTime());
        stationUsageEntity.setTotalDuration(stationUsageRequest.getTotalDuration());

        return stationUsageEntity;
    }
}
