package com.intel.vehicle_charging_station.api.controller;

import com.intel.vehicle_charging_station.api.repository.DayInfo;
import com.intel.vehicle_charging_station.api.service.DayInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
public class DayController {
    private DayInfoService dayInfoService;

    public DayController(DayInfoService dayInfoService) {
        this.dayInfoService = dayInfoService;
    }

    @GetMapping("/days")
    public List<DayInfo> getDayInfo(LocalDate from, LocalDate to) {
        return dayInfoService.getDayInfo(from, to);
    }
}
