package com.intel.vehicle_charging_station.api.controller;

import com.intel.vehicle_charging_station.api.repository.StationUsageEntity;
import com.intel.vehicle_charging_station.api.service.StationUsageService;
import com.intel.vehicle_charging_station.api.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController()
public class StationUsageController {
    private StationUsageService stationUsageService;
    private StationUsageMapper stationUsageMapper;

    public StationUsageController(StationUsageService stationUsageService, UserService userService, StationUsageMapper stationUsageMapper) {
        this.stationUsageService = stationUsageService;
        this.stationUsageMapper = stationUsageMapper;
    }

    @GetMapping("/station-usages")
    public Page<StationUsageEntity> getStationUsages(@RequestParam(required = false) String macAddress,
                                                     @RequestParam(required = false) String userId,
                                                     @RequestParam(required = false) Double minEnergy,
                                                     @RequestParam(required = false) Double maxEnergy,
                                                     @RequestParam(defaultValue = "0") Integer page,
                                                     @RequestParam(defaultValue = "10") Integer size) {

        return stationUsageService.getAll(macAddress, userId, minEnergy, maxEnergy, page, size);
    }

    @GetMapping("/station-usages/{id}")
    public StationUsageEntity getStationUsage(@PathVariable("id") Long id) {
        return stationUsageService.getById(id);
    }

    @PostMapping("/station-usages")
    public StationUsageEntity createStationUsage(@RequestBody StationUsageRequest stationUsageRequest) {
        StationUsageEntity stationUsageEntity = stationUsageMapper.map(stationUsageRequest);
        return stationUsageService.create(stationUsageEntity);
    }
}
