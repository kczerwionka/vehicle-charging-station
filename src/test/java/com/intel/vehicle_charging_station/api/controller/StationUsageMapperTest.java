package com.intel.vehicle_charging_station.api.controller;

import com.intel.vehicle_charging_station.api.repository.StationUsageEntity;
import org.junit.jupiter.api.Test;

import java.sql.Time;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

class StationUsageMapperTest {

    @Test
    public void shouldMapStationUsageRequestToStationUsageEntity() {
        // given
        StationUsageRequest stationUsageRequest = new StationUsageRequest();
        stationUsageRequest.setStationName("PALO ALTO CA / HAMILTON #2");
        stationUsageRequest.setChargingTime(new Time(3600000));
        stationUsageRequest.setEndDate(LocalDateTime.of(2021,3,4, 14, 20, 7));
        stationUsageRequest.setEndTimeZone("PL");
        stationUsageRequest.setStartDate(LocalDateTime.of(2021,3,4, 12, 20, 0));
        stationUsageRequest.setStartTimeZone("PL");
        stationUsageRequest.setEnergy(8.25);
        stationUsageRequest.setMacAddress("000D:6F00:009E:D39E");
        stationUsageRequest.setUserId("1234");
        stationUsageRequest.setTotalDuration(new Time(3650000));
        stationUsageRequest.setTransactionDate(LocalDateTime.of(2021,3,4, 14, 21, 17));
        StationUsageMapper stationUsageMapper = new StationUsageMapper();

        // when
        StationUsageEntity result = stationUsageMapper.map(stationUsageRequest);

        // then
        assertThat(result.getStationName()).isEqualTo("PALO ALTO CA / HAMILTON #2");
        assertThat(result.getChargingTime()).isEqualTo(new Time(3600000));
        assertThat(result.getEndDate()).isEqualTo(LocalDateTime.of(2021,3,4, 14, 20, 7));
        assertThat(result.getEndTimeZone()).isEqualTo("PL");
        assertThat(result.getStartDate()).isEqualTo(LocalDateTime.of(2021,3,4, 12, 20, 0));
        assertThat(result.getStartTimeZone()).isEqualTo("PL");
        assertThat(result.getEnergy()).isEqualTo(8.25);
        assertThat(result.getMacAddress()).isEqualTo("000D:6F00:009E:D39E");
        assertThat(result.getUserId()).isEqualTo("1234");
        assertThat(result.getTotalDuration()).isEqualTo(new Time(3650000));
        assertThat(result.getTransactionDate()).isEqualTo(LocalDateTime.of(2021,3,4, 14, 21, 17));
    }

}