package com.intel.vehicle_charging_station.api.service;

import com.intel.vehicle_charging_station.api.repository.StationUsageEntity;
import com.intel.vehicle_charging_station.api.repository.StationUsageRepository;
import com.intel.vehicle_charging_station.api.service.exception.StationUsageNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.Optional;

import static com.intel.vehicle_charging_station.api.service.StationUsageSpecifications.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StationUsageServiceTest {
    @Mock
    private StationUsageRepository stationUsageRepository;
    @InjectMocks
    private StationUsageService stationUsageService;

    @Test
    public void shouldGetAllWithMacAddressFilter() {
        // given
        String macAddress = "000D:6F00:015A:9D76";
        Specification<StationUsageEntity> specification = Specification.where(null);
        specification = specification.and(macAddressEquals(macAddress));

        when(stationUsageRepository.findAll(refEq(specification), eq(PageRequest.of(0, 10)))).thenReturn(Page.empty());

        // when
        Page<StationUsageEntity> result = stationUsageService.getAll(macAddress, null, null, null, 0, 10);

        // then
        assertThat(result.getTotalPages()).isEqualTo(1);
    }

    @Test
    public void shouldGetAllWithUserIdAndMinEnergyFilter() {
        // given
        String macAddress = "000D:6F00:015A:9D76";
        Double minEnergy = 4.0;
        Specification<StationUsageEntity> specification = Specification.where(null);
        specification = specification.and(macAddressEquals(macAddress));
        specification = specification.and(minEnergy(minEnergy));

        when(stationUsageRepository.findAll(refEq(specification), eq(PageRequest.of(0, 10)))).thenReturn(Page.empty());

        // when
        Page<StationUsageEntity> result = stationUsageService.getAll(macAddress, null, minEnergy, null, 0, 10);

        // then
        assertThat(result.getTotalPages()).isEqualTo(1);
    }

    @Test
    public void shouldThrowExceptionWhenIdStationUsageDoesNotExist() {
        // given
        Long id = 12l;
        when(stationUsageRepository.findById(id)).thenReturn(Optional.empty());
        String expectedMessage = "Station usage not found for id: 12";

        // when
        Exception exception = assertThrows(StationUsageNotFoundException.class, () -> {
            stationUsageService.getById(id);
        });

        // then
        assertThat(expectedMessage).isEqualTo(exception.getMessage());
    }

}