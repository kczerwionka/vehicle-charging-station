# API #2 #

Internal Api to get data about historical usage data of EV (electrical vehicle)
charging stations in Palo Alto (US)

### How can I start service? ###

* set proper value in ```application.properties``` to connect to MySQL
* ```mvn clean install```
* ```mvn spring-boot:run```

### Endpoints ###

* ```GET /days``` - returns customer who consumed the most energy and energy which the customer consumed per specific day
  eg. ```http://localhost:8080/days```

* ```GET /station-usages``` - returns data for each usage of charging station (possible filters: macAddress, userId, 
  minEnergy, maxEnergy and paging by page (default 0) and size(default 10))
  eg. ```http://localhost:8080/station-usages?minEnergy=12&userId=2670&maxEnergy=12.3```
  and with paging ```http://localhost:8080/station-usages?minEnergy=12&userId=2670&maxEnergy=12.3&page=1&size=3```

* ```POST /station-usages``` - save new entry about charging station
eg. ```curl -i -X POST -H "Content-Type: application/json" -d '{"stationName":"test", "macAddress":"1111", "startDate":"2011-11-09T22:46:00", "startTimeZone":"PL", "endDate":"2011-11-10T06:46:00", "endTimeZone":"PL", "transactionDate":"2011-11-10T06:46:00", "totalDuration":"05:00:00", "chargingTime":"08:00:00", "energy":10, "userId":"1234"}' http://localhost:8080/station-usages```

* ```GET /users``` - returns data about user usage like averageEnergyPerOneCharging, numberOfCharging, sumOfConsumedEnergy
  eg. ```http://localhost:8080/users```